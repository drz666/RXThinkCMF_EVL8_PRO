<?php


namespace App\Http\Controllers;

use App\Services\ConfigWebService;

/**
 * 网站配置-控制器
 * @author 牧羊人
 * @since 2021/6/28
 * Class ConfigWebController
 * @package App\Http\Controllers
 */
class ConfigWebController extends Backend
{
    /**
     * 构造函数
     * @author 牧羊人
     * @since 2021/6/28
     * ConfigWebController constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->service = new ConfigWebService();
    }

    /**
     * 保存配置信息
     * @return array|mixed
     * @since 2021/6/28
     * @author 牧羊人
     */
    public function edit()
    {
        $result = $this->service->edit();
        return $result;
    }

}
