<?php
// +----------------------------------------------------------------------
// | RXThinkCMF框架 [ RXThinkCMF ]
// +----------------------------------------------------------------------
// | 版权所有 2017~2021 南京RXThinkCMF研发中心
// +----------------------------------------------------------------------
// | 官方网站: http://www.rxthink.cn
// +----------------------------------------------------------------------
// | Author: 牧羊人 <1175401194@qq.com>
// +----------------------------------------------------------------------

namespace App\Services;

use App\Models\DictDataModel;
use App\Models\DictModel;

/**
 * 字典数据-服务类
 * @author 牧羊人
 * @since 2020/11/11
 * Class DictService
 * @package App\Services
 */
class DictDataService extends BaseService
{
    /**
     * 构造函数
     * @author 牧羊人
     * @since 2020/11/11
     * DictService constructor.
     */
    public function __construct()
    {
        $this->model = new DictDataModel();
    }

    /**
     * 获取字典列表
     * @return array
     * @since 2020/11/11
     * @author 牧羊人
     */
    public function getList()
    {
        $param = request()->all();
        // 查询条件
        $map = [];
        // 字典ID
        $dictId = getter($param, "dictId", 0);
        if ($dictId) {
            $map[] = ['dict_id', '=', $dictId];
        }
        // 字典名称
        $name = getter($param, "name");
        if ($name) {
            $map[] = ['name', 'like', "%{$name}%"];
        }
        // 字典编码
        $code = getter($param, 'code');
        if ($code) {
            $map[] = ['code', '=', $code];
        }
        $list = $this->model->getList($map, [['sort', 'asc']]);
        return message("操作成功", true, $list);
    }

    /**
     * 根据Code获取字典
     * @return array
     * @since 2021/7/12
     * @author 牧羊人
     */
    public function getDictByCode()
    {
        // 参数
        $param = request()->all();
        // Code码
        $code = getter($param, "code");
        if (!$code) {
            return message("参数不能为空", false);
        }
        $dictModel = new DictModel();
        $dictInfo = $dictModel->getOne([
            ['code', "=", $code],
        ]);
        if (!$dictInfo) {
            return message("字典信息不存在", false);
        }
        // 获取字典项列表
        $list = $this->model->where("dict_id", "=", $dictInfo['id'])
            ->where("mark", "=", 1)
            ->get()
            ->toArray();
        return message("操作成功", true, $list);
    }

}
